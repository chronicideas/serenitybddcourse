Feature: Order a coffee

  In order to save time when I pick up my morning coffee
  As a coffee lover
  I want to be able tto order my coffee in advance

  Scenario: 01. Buyer orders a coffee
    Given Emily has a Caffeinate-Me account
    When she orders a large cappucino
    Then Thomas should receive the order

  Scenario: 02. Prioritise orders according to client ETA
    Given S