package caffeinateme.stepdefs;

import caffeinateme.OrderReceipt;
import caffeinateme.serenitysteps.Barista;
import caffeinateme.serenitysteps.Customer;
import caffeinateme.serenitysteps.Order;
import caffeinateme.serenitysteps.UserRegistrationClient;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderCoffeeSteps {

    @Steps
    private UserRegistrationClient userRegistrations;

    @Steps
    private Customer emily;

    @Steps
    private
    Barista thomas;

    private OrderReceipt orderReceipt;

    @Given("(?:.*) has a Caffeinate-Me account")
    public void userHasACaffeinateMeAccount() {
        userRegistrations.registerUser(emily);
    }

    @When("^s?he orders a (.*)$")
    public void sheOrdersALargeCappucino(String order) {
        orderReceipt = emily.placesAnOrderFor(1, order);
    }

    @Then("^Thomas should receive the order$")
    public void thomasShouldReceiveTheOrder() {
        assertThat(thomas.pendingOrders()).contains(Order.matching(orderReceipt));
    }
}
